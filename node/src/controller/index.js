import userController from './user.controller.js'

export default app => app.use('/users', userController)
