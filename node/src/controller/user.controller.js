import userRespository from '../repository/user.repository.js'

import { Router } from 'express'


const userController = Router()

userController.get('/', (_, res) => {
    userRespository.list()
        .then(users => users.reduce((element, user) => element.concat(`<li>${user.name}</li>\n`), ''))
        .then(names => `
            <h1>Full Cycle Rocks!</h1>
            <ul>
                ${names}
            </ul>
            `)
        .then(response => res.send(response))
})

export default userController