import app from './config/rest.config.js'
import controller from './controller/index.js'
const PORT = 3000

controller(app)

app.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`)
})