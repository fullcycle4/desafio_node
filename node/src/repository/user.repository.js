import db from '../config/db.config.js'

async function list() {
    return new Promise(resolve => {
        db.query('SELECT * FROM user', (error, results) => {
            if (error) throw error
            resolve(results)
        })
    })
}

async function save(user) {
    return Promise.resolve(user)
}

export default {
    list, save
}