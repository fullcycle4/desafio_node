import mysql from 'mysql'
var connection = mysql.createConnection({
    host: 'mysql',
    user: 'fullcycle',
    password: 'fullcycle',
    database: 'fullcycle'
})

connection.connect(error => {
    if(error) throw error
    else console.log(`Database connected on id: ${connection.threadId}`)
})

export default connection
