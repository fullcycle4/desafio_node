CREATE TABLE user(
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO user (name) VALUES
('Fullcycle User'),
('Weslley Willians'),
('Luiz Carlos'),
('Luiz Henrique'),
('Lucian Silva');